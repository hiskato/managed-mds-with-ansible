# Managed MDS with Ansible
This repository contains sample ansible script for MDS.

DISCLAIMER: This is NOT an official Cisco repository and comes with NO WARRANTY AND/OR SUPPORT
Please check LICENSE-CISCO for additional details

## Getting started
Manage your MDS enviroment with Ansible. You will see easy-to-manage for SAN with ansible.

1. Device Aliaas Management 
2. Zone Management
3. Zoneset Management

This script intend to manage MDS with "add" and "remove" operation for the above the operations. 

### How to invoke the script

You need to add variable to files under var directory as you want to set up the zoneset, devalias and zone. You need to define yaml format so that ansible can understand variable. It is basically you need to create var file per MDS Device.

```
devalias:
- name: netapp-fc0
  pwwn: 20:02:00:a0:98:e5:da:c6
  vsan: 2001

zoneset:
- name: mds-hol-a-zoneset
  vsan: 2001
  members:
  - name: mds-hol-a-zone

zone:
- member: netapp-fc0
  name: mds-hol-a-zone
  vsan: 2001
```

Simply you can invoke the script as follows.

1. How to add the device alias to the specific switch
```
ansible-playbook -i inventory device.yml -e 'flag=add mdshosts=mds9148-01'
ansible-playbook -i inventory device.yml -e 'flag=add mdshosts=mds9148-02'
```
2. How to create the zone and zoneset in the specific switch
```
ansible-playbook -i inventory zone.yml -e 'flag=add mdshosts=mds9148-01'
ansible-playbook -i inventory zone.yml -e 'flag=add mdshosts=mds9148-02'
```

