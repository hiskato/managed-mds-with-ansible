#!/bin/sh -x 

remove(){
ansible-playbook -i inventory device.yml -e 'flag=remove mdshosts=mds9148-01'
ansible-playbook -i inventory device.yml -e 'flag=remove mdshosts=mds9148-02'
}

add(){
ansible-playbook -i inventory device.yml -e 'flag=add mdshosts=mds9148-01'
ansible-playbook -i inventory device.yml -e 'flag=add mdshosts=mds9148-02'
}

if [ "$#" = "1" ];then
  if [ "$1" = "add" ];then
    add
  elif [ "$1" = "remove" ];then
    remove
  fi
else
 echo "./hol.sh [add/remove]"
fi